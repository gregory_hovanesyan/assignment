export default {
	avatarSrc: state => (size = '') => {
		return require(`~/assets/images/${state.user.avatar +
		(size && '-') + // prefix size with a hyphen if a size is passed
			size}.jpg`)
	},
	avatarSrcSet: state => (size = '') => {
		return `${require(`~/assets/images/${state.user.avatar +
			(size && '-') +
			size}@2x.jpg`)} 2x,
					${require(`~/assets/images/${state.user.avatar +
						(size && '-') +
						size}@3x.jpg`)} 3x`
	}
}
