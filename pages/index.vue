<template>
	<div class="app h-full">
		<Menu />
		<div class="container">
			<Breadcrumb to="/" class="fade-in">Matchmaking Lobby</Breadcrumb>
			<div class="title-large-dark flex items-center mt-4 mb-3 fade-in">
				{{ user }}’s lobby
				<IconLock class="ml-4" />
			</div>
			<div class="flex fade-in">
				<GLabel text="FIFA 19" />
			</div>
			<div class="mt-6 flex">
				<Player
					:name="user"
					:picture="avatar"
					position="Team Leader"
					class="fade-in"
					:g-score="74"
					:is-ready="true"
				/>
				<Player class="fade-in" />
			</div>
			<div class="absolute w-full left-0 bottom-0 pl-4 pr-4 bg-sand">
				<Accordion
					name="Type"
					:items="typeData"
					:class="{ open: open === 'type' }"
					class="fade-in"
					@click.native="toggleSelector('type')"
					@select="select('type', $event)"
				/>
				<Accordion
					name="Gamemode"
					:items="modeData"
					:class="{ open: open === 'mode' }"
					class="fade-in"
					@click.native="toggleSelector('mode')"
					@select="select('mode', $event)"
				/>
				<Accordion
					name="Lobby Status"
					:items="lobbyData"
					:class="{ open: open === 'lobby' }"
					class="fade-in"
					@click.native="toggleSelector('lobby')"
					@select="select('lobby', $event)"
				/>
			</div>
		</div>
		<ButtonResponsive
			to="/searching"
			call-to-action="Play Now"
			class="fade-in"
		>
			{{ inQueueCount | comma }} in Queue
		</ButtonResponsive>
	</div>
</template>
<script>
import { TimelineLite, Power2 } from 'gsap'
import Menu from '../components/layout/menu'
import IconLock from '../components/icons/Lock'
import Player from '../components/player'
import GLabel from '../components/ui/label'
import Breadcrumb from '../components/ui/breadcrumb'
import ButtonResponsive from '../components/ui/buttonResponsive'
import Accordion from '../components/ui/accordion'

export default {
	layout: 'holder',
	components: {
		GLabel,
		IconLock,
		Menu,
		Player,
		Breadcrumb,
		ButtonResponsive,
		Accordion
	},
	filters: {
		comma(num) {
			return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')
		}
	},
	data() {
		return {
			user: 'steveroesler',
			avatar: 'gf-avatar-01',
			inQueueCount: '4021',
			open: false,
			selected: {
				type: 'Competitive',
				mode: 'Squad Battles',
				lobby: 'Invite Only'
			},
			data: {
				type: ['Competitive', 'Standard'],
				mode: [
					'Squad Battles',
					'Ultimate Team',
					'Kick Off',
					'Skill Games',
					'Practice Arena'
				],
				lobby: ['Invite Only', 'Public']
			}
		}
	},
	computed: {
		// these methods sort selector list
		// to make the selected element first
		typeData() {
			return [
				this.selected.type,
				...this.data.type.filter(item => item !== this.selected.type)
			]
		},
		modeData() {
			return [
				this.selected.mode,
				...this.data.mode.filter(item => item !== this.selected.mode)
			]
		},
		lobbyData() {
			return [
				this.selected.lobby,
				...this.data.lobby.filter(item => item !== this.selected.lobby)
			]
		}
	},
	mounted() {
		const timeline = new TimelineLite()

		timeline.staggerFrom(
			'.fade-in',
			1,
			{ opacity: 0, ease: Power2.ease },
			0.1
		)
	},
	methods: {
		toggleSelector(selector) {
			this.open = selector === this.open ? false : selector
		},
		select(selector, value) {
			this.selected[selector] = value
		}
	}
}
</script>
<style lang="scss" scoped>
.container {
	max-width: 100%;
	padding-left: 16px;
	padding-right: 16px;
	position: relative;
	height: calc(100% - 89px - 105px);
}

.page-leave-active {
	transition: background-position 0.3s ease-out;
}
</style>
